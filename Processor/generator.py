fout = open("generated_file.cpp", "w")
fin = open("../keywords", "r")
lines = fin.readlines()
print("void (StackProcessor::*_handlers[40])() = {", file=fout)
for i in lines:
    print("    &StackProcessor::", i.split()[0], "_handler,", sep='', file=fout)
print("};\nfor (int i = 0; i < ", len(lines), "; i++) {\n    handlers[i] = _handlers[i];\n}\n", file=fout, sep='')
fout.close()
fin.close()