unsigned long long __hash(const void *ptr, int size) {
    const char *tmp = (const char *)ptr;
    unsigned long long _h = 0;
    unsigned int p = 307;
    for (int i = 0; i < size; i++) {
        _h = _h * p + tmp[i];
    }
    return _h;
}

template <class T, int N>
bool SafeStack<T, N>::OK() {
    if (!this || !_st)
        return false;
    return _st->OK();
}

template <class T, int N>
void SafeStack<T, N>::defend() {
    _st->_hash = 0;
    _st->_hash = __hash(_st, sizeof(SafeStackData<T, N>) - N * sizeof(T) + _st->_max_size * sizeof(T));
}

template <class T, int N>
bool SafeStack<T, N>::is_empty() const {
    if (!this) assert(!"SafeStack is null");
    ASSERT_OK(this->_st, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    return _st->size == 0;
}

template <class T, int N>
SafeStackData<T, N>::SafeStackData(bool is_extandable)
        : canary1(OK_CANARY),
          is_extendable(is_extandable),
          size(0),
          _max_size(N),
          last_error_code(OK_CODE)
{
    for (int i = 0; i < N; i++)
        buffer[i] = POISON<T>();

    can_ptr = (int *)((char *)buffer + N * sizeof(T));
    *can_ptr = OK_CANARY;
    _hash = 0;
    _hash = __hash(this, sizeof(SafeStackData<T, N>));
}

template <class T, int N>
bool SafeStackData<T, N>::OK() {
    if (canary1 != OK_CANARY || *(can_ptr) != OK_CANARY)
        return false;

    if (size < 0 || size > _max_size)
        return false;

    for (int i = size; i < _max_size; i++)
        if (buffer[i] != POISON<T>())
            return false;

    unsigned long long shash = _hash;
    _hash = 0;
    if (__hash(this, sizeof(SafeStackData<T, N>) - sizeof(T) * N + _max_size * sizeof(T)) != shash) {

        _hash = shash;
        return false;
    }
    _hash = shash;
    return true;
}

#ifdef SUPER_DEBUG
template <class T, int N>
SafeStackData<T, N>::SafeStackData(const char name[], bool is_extandable)
    : canary1(OK_CANARY), 
      is_extendable(is_extandable),
      size(0), 
      _max_size(N),
      last_error_code(OK_CODE) {
  strcpy(var_name, name);
  for (int i = 0; i < N; i++) {
    buffer[i] = POISON<T>();
  }
  can_ptr = (int *)((char *)this + sizeof(SafeStackData<T, N>)); 
  *can_ptr = OK_CANARY;
  _hash = 0;
  _hash = __hash(this, sizeof(SafeStackData<T, N>) - sizeof(T) * N + _max_size * sizeof(T));
}
#endif

template <class T, int N>
SafeStack<T, N>::SafeStack() {
    char* _buf = (char*)calloc(1, sizeof(SafeStackData<T, N>) + sizeof(int) + 1);
    SafeStackData<T, N>* st = new(_buf) SafeStackData<T, N>;

    _st = st;
}

#ifdef SUPER_DEBUG
template <class T, int N>
SafeStack<T, N>::SafeStack(const char name[]) {
  char* _buf = (char *)calloc(1, sizeof(SafeStackData<T, N>) + sizeof(int) + 1);
  SafeStackData<T, N>* st = new(_buf) SafeStackData<T, N>(name);

  _st = st;
}

template <class T, int N>
SafeStack<T, N>::SafeStack(const char name[], SafeStackData<T, N>* data_ptr) {
  strcpy(data_ptr->var_name, name);
  _st = data_ptr;
}
#endif

template <class T, int N>
SafeStack<T, N>::SafeStack(SafeStackData<T, N>* data_ptr) : _st(data_ptr) {}


template <class T, int N>
int SafeStack<T, N>::push(const T& item) {
    if (!this) assert(!"SafeStack is empty");
    _ass
    if (_st->size == _st->_max_size) {
        if (_st->is_extendable) {
            auto tmp = reall();
            free(_st);
            _st = tmp;
#ifdef SUPER_DEBUG
            _asserter._setStackData(_st);
#endif
        } else {
            _st->last_error_code = OVERFLOW_WHEN_PUSH;
            DDUMP()
            assert(!"Buffer overflow when trying to push");
            return _st->last_error_code;
        }
    }
    _st->buffer[_st->size] = item;
    ++_st->size;
    defend();
    return OK_CODE;
}

template <class T, int N>
SafeStackData<T, N>* SafeStack<T, N>::reall() {
    _ass
    char *buf = (char *)calloc((sizeof(SafeStackData<T, N>) -
                                N * sizeof(T) +
                                2 * _st->_max_size * sizeof(T)
                                + sizeof(int)) + 1, 1);
    SafeStackData<T, N>* new_st = new(buf) SafeStackData<T, N>;
#ifdef SUPER_DEBUG
    strcpy(new_st->var_name, _st->var_name);
#endif
    new_st->size = _st->size;
    new_st->_max_size = _st->_max_size * 2;
    new_st->can_ptr = (int *)((char *)new_st->buffer + sizeof(T) * new_st->_max_size);
    *(new_st->can_ptr) = OK_CANARY;

    for (int i = 0; i < _st->_max_size; i++)
        new_st->buffer[i] = _st->buffer[i];

    for (int i = 0; i < _st->_max_size / 2; i++)
        new_st->buffer[_st->_max_size + i] = POISON<T>();

    return new_st;
}

template <class T, int N>
int SafeStack<T, N>::pop() {
    if (!this) assert(!"SafeStack is empty");
    _ass
    if (_st->size == 0) {
        _st->last_error_code = EMPTY_WHEN_POP;
        DDUMP()
        assert(!"Buffer empty when trying to pop");
        return _st->last_error_code;
    }
    --_st->size;
    _st->buffer[_st->size] = POISON<T>();
    defend();
    return OK_CODE;
}

template <class T, int N>
T SafeStack<T, N>::top() const {
    if (!this) assert(!"SafeStack is empty");
    _ass
    if (_st->size == 0) {
        _st->last_error_code = EMPTY_WHEN_TOP;
        DDUMP()
        assert(!"Buffer empty when trying to get top");
        return _st->last_error_code;
    }
    return _st->buffer[_st->size - 1];
}

template <class T, int N>
SafeStack<T, N>::~SafeStack() {
    if (_st->is_extendable)
        free(_st);
}