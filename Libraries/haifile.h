//
// Created by dm on 10/20/19.
//

#ifndef UNTITLED_HAIFILE_H
#define UNTITLED_HAIFILE_H
#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cassert>

/*!
  \brief String without possession
  Light string that use external buffer to store chars.
 */
struct light_string {
    char *ptr;
    int length;
    const char &operator[](int index) const;
    bool operator==(const light_string &str) const;
};

/*!
  Get char from light string read-only.
  \param[in] index Position of char
*/
const char &light_string::operator[](int index) const {
    assert(this);
    assert(index < length);
    assert(index >= 0);
    return *(this->ptr + index);
}

bool light_string::operator==(const light_string &str) const {
    if (length != str.length)
        return false;
    for (int i = 0; i < length; i++)
        if ((*this)[i] != str[i])
            return false;
    return true;
}
/*!
  Struct to hash light_string
*/
struct LightStringDoubleHashFunc {
    size_t operator()(const light_string &str, size_t probe = 0) const;
    LightStringDoubleHashFunc(size_t M);

private:
    size_t M, a1, a2;
    size_t first_hash(const light_string &str) const;
    size_t second_hash(const light_string &str) const;
    size_t string_hash(const light_string &str, size_t a) const;
};

LightStringDoubleHashFunc::LightStringDoubleHashFunc(size_t M) : M(M) {
    a1 = 257;  // Можно брать любые нечетные
    a2 = 263;
}

size_t LightStringDoubleHashFunc::operator()(const light_string &str, size_t probe) const {
    return (first_hash(str) + ((probe * second_hash(str)) % M)) % M;
}

size_t LightStringDoubleHashFunc::first_hash(const light_string &str) const {
    return string_hash(str, a1);
}

size_t LightStringDoubleHashFunc::second_hash(const light_string &str) const {
    return ((string_hash(str, a2) << 1) | 1) % M;
}

size_t LightStringDoubleHashFunc::string_hash(const light_string &str, size_t a) const {
    size_t ans = 0;
    for (int i = 0; i < str.length; i++) {
        ans = (ans * a + str[i]) % M;
    }
    return ans;
}

/*!
  Read all characters from file
  \param file Input file
  \param[out] size Size of text
  \return buffer of all characters
*/
char *read_file(const char* filename, int *size) {
    assert(filename);

    FILE *input_file = fopen(filename, "r");
    assert(input_file);

    fseek(input_file, 0, SEEK_END);
    int file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    char *buffer = (char *)calloc(file_size + 1, sizeof(char));
    assert(buffer);

    file_size = fread(buffer, 1, file_size, input_file);

    buffer[file_size] = '\n';
    *size = file_size + 1;

    fclose(input_file);
    return buffer;
}

/*!
  Divide buffer from file into lines
  \param buffer buffer with characters
  \param size buffer size
  \param[out] lines_number number of lines in file
  \return array of lines
*/
light_string *divide_text(char *buffer, int size, int *lines_number) {
    assert(buffer);

    int string_count = 1;
    for (int i = 0; i < size; i++) {
        if (buffer[i] == '\n') {
            string_count++;
        }
    }

    light_string *text =
            (light_string *)calloc(string_count + 1, sizeof(light_string));
    text[string_count].ptr = nullptr;
    text[0].ptr = buffer;
    int current_line = 1;
    for (int i = 0; i < size; i++) {
        if (buffer[i] == '\n') {
            text[current_line].ptr = (buffer + i + 1);
            text[current_line - 1].length =
                    text[current_line].ptr - text[current_line - 1].ptr - 1;

            current_line++;
            buffer[i] = '\0';
        }
    }

    *lines_number = string_count - 1;
    return text;
}

#endif //UNTITLED_HAIFILE_H
