//
// Created by dm on 10/21/19.
//

#ifndef UNTITLED_STACK_H
#define UNTITLED_STACK_H
/*!
  \file
  \brief Stack
  Safe stack with autocontrol
 */

#pragma once

#define SUPER_DEBUG
//#undef SUPER_DEBUG
#include <assert.h>
#include <string.h>
#include <string>

#ifdef SUPER_DEBUG
#include <cxxabi.h>
#include <iostream>
#endif

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"


#ifdef SUPER_DEBUG

#define CREATE_STACK(name, T, N) SafeStack<T, N> name(#name);

#define CREATE_STATIC_STACK(name, T, N) SafeStackStatic<T, N> name(#name);

#define _ass                                                                   \
  _asserted<T, N> _asserter(this->_st, __FILE__, __LINE__, __PRETTY_FUNCTION__);

#define ASSERT_OK(stack_ptr, file, line, funct)                                \
  if (!stack_ptr || !stack_ptr->OK()) {                                        \
    DUMP(stack_ptr, file, line, funct);                                        \
    assert(!"stack ok failed");                                                \
  }

#define DDUMP() DUMP(this->_st, __FILE__, __LINE__, __PRETTY_FUNCTION__)

#define DUMP(stack_ptr, file, line, funct)                                     \
  printf(ANSI_COLOR_RED "Ok failed!!!!" ANSI_COLOR_RESET                       \
                        "from %s(%d) in %s\n",                                 \
                        file, line, funct);                                    \
  if (!stack_ptr)                                                              \
    printf("Stack_ptr is NULL\n");                                             \
  else {                                                                       \
    int status = 0;                                                            \
    char *demangled =                                                          \
        abi::__cxa_demangle(typeid(*stack_ptr).name(), 0, 0, &status);         \
    printf("%s %s [0x%x] \n{\n", demangled, stack_ptr->var_name, stack_ptr);   \
    printf("  er_code = %d", stack_ptr->last_error_code);                      \
    free(demangled);                                                           \
    switch (stack_ptr->last_error_code) {                                      \
    case OK_CODE:                                                              \
      printf(ANSI_COLOR_GREEN " (OK)\n" ANSI_COLOR_RESET);                     \
      break;                                                                   \
    case OVERFLOW_WHEN_PUSH:                                                   \
      printf(ANSI_COLOR_RED " (stack overflow when push)\n" ANSI_COLOR_RESET); \
      break;                                                                   \
    case EMPTY_WHEN_POP:                                                       \
      printf(ANSI_COLOR_RED " (buffer empty when pop)\n" ANSI_COLOR_RESET);    \
      break;                                                                   \
    case EMPTY_WHEN_TOP:                                                       \
      printf(ANSI_COLOR_RED " (buffer empty when top)\n" ANSI_COLOR_RESET);    \
      break;                                                                   \
    }                                                                          \
    printf("  canary1 = %d (%s)\n", stack_ptr->canary1,                        \
           stack_ptr->canary1 == OK_CANARY                                     \
               ? ANSI_COLOR_GREEN "OK"  ANSI_COLOR_RESET                       \
               : ANSI_COLOR_RED   "BAD" ANSI_COLOR_RESET);                     \
    printf("  canary2 = %d (%s)\n", *(stack_ptr->can_ptr),                     \
               *(stack_ptr->can_ptr) == OK_CANARY                              \
               ? ANSI_COLOR_GREEN "OK"  ANSI_COLOR_RESET                       \
               : ANSI_COLOR_RED   "BAD" ANSI_COLOR_RESET);                     \
    printf("  size = %d\n", stack_ptr->size);                                  \
    printf("  old_hash = %ull (current_hash = %ull)\n", stack_ptr->_hash,      \
           __hash(stack_ptr->buffer, sizeof(T) * stack_ptr->_max_size));       \
    printf("  data[%d] [0x%x]\n  {\n", stack_ptr->_max_size,                   \
           stack_ptr->buffer);                                                 \
    for (int i = 0; i < stack_ptr->size; i++) {                                \
      printf("    [%d] = ", i);                                                \
      std::cout << stack_ptr->buffer[i] << "\n";                               \
    }                                                                          \
    for (int i = stack_ptr->size; i < stack_ptr->_max_size; i++) {             \
      printf("    *[%d] = ", i);                                               \
      std::cout << stack_ptr->buffer[i];                                       \
      printf(" (%s)\n", stack_ptr->buffer[i] == stack_ptr->_poison             \
                            ? "POISON"                                         \
                            : "NOT POISON");                                   \
    }                                                                          \
    printf("  }\n}\n");                                                        \
  }
#endif

#ifndef SUPER_DEBUG
#define CREATE_STACK(name, T, N) SafeStack<T, N> name;
#define CREATE_STATIC_STACK(name, T, N) SafeStackStatic<T, N> name;

#define DDUMP() ;
#define _ass ;
#define DUMP(stack_ptr, file, line, funct) ;
#define ASSERT_OK(stack_ptr, file, line, funct) ;
#endif

unsigned long long __hash(const void *ptr, int size);

const int OK_CANARY = 12345678;

typedef enum {
    OK_CODE = 0,
    OVERFLOW_WHEN_PUSH = 1,
    EMPTY_WHEN_POP = 2,
    EMPTY_WHEN_TOP = 3
} ERROR_CODE;

/*!
  \brief POISON class
  Poison class for testing purposes only
 */
template <typename T> struct POISON {
    operator T() { return T(); }
};

template <class T, int N> struct _asserted;
template <class T, int N> class SafeStack;
/*!
  \brief Safe stack data
  Safe stack data in static or dynamic memory
*/
template <class T, int N> struct SafeStackData {
private:
    int canary1;
    bool is_extendable;
public:
    int* can_ptr;
private:
    unsigned long long _hash;
    int size;
    int _max_size;
    mutable ERROR_CODE last_error_code;

    friend struct _asserted<T, N>;
    friend class SafeStack<T, N>;

#ifdef SUPER_DEBUG
    T _poison = POISON<T>();
public:
    char var_name[256];
private:
#endif

    T buffer[N];
public:
    SafeStackData(bool is_extendable = true);
    bool OK();
#ifdef SUPER_DEBUG
    SafeStackData(const char var_name[], bool is_extendable = true);
#endif
};

/*!
  \brief Safe stack in dynamic memory
  Safe stack with auto dumping
*/
template <class T, int N> class SafeStack {
private:
    SafeStackData<T, N> *_st;

    /*!
      Defend stack from editing
    */
    void defend();

    /*!
      Reallocate new memory if extedible
    */
    SafeStackData<T, N>* reall();
public:
    SafeStack();
    SafeStack(SafeStackData<T, N>* data_ptr);
    void Dump(const char *file, int line, const char *func) {
        DUMP(_st, file, line, func);
    }
#ifdef SUPER_DEBUG
    SafeStack(const char var_name[]);
    SafeStack(const char var_name[], SafeStackData<T, N>* data_ptr);
#endif
    /*!
      Check stack is empty.
    */
    bool is_empty() const;

    bool OK();

    /*!
      Push element to stack.
      \param T item to be pushed
    */
    int push(const T &item);

    /*!
      Pop element from stack.
    */
    int pop();

    /*!
      Top element of stack.
    */
    T top() const;

    ~SafeStack();
};

/*!
  \brief Safe stack in static memory
  Safe stack wrapper with static memory
*/
template <class T, int N>
class SafeStackStatic : public SafeStack<T, N> {
private:
    SafeStackData<T, N> _data;
    int canary2;
public:
    SafeStackStatic():
            _data(false),
            SafeStack<T, N>(&_data),
            canary2(OK_CANARY) {}
#ifdef SUPER_DEBUG
    SafeStackStatic(const char var_name[]) :
            _data(false),
            SafeStack<T, N>(var_name, &_data),
            canary2(OK_CANARY) {}
#endif

};

#ifdef SUPER_DEBUG
/*!
  \brief asserter to stack
  Checks assert_ok for stack when construct and delete.
 */
template <class T, int N> struct _asserted {
    SafeStackData<T, N> *stack_ptr;
    const char *filename;
    const char *functname;
    int lineno;
    _asserted(SafeStackData<T, N> *ptr, const char filename[], int lineno,
              const char functname[])
            : stack_ptr(ptr),
              filename(filename),
              functname(functname),
              lineno(lineno)
    {
        ASSERT_OK(ptr, filename, lineno, functname);
    }
    void _setStackData(SafeStackData<T, N> *new_ptr) {stack_ptr = new_ptr;}
    ~_asserted() { ASSERT_OK(stack_ptr, filename, lineno, functname); }
};
#endif

#include "stack.cpp"
#endif //UNTITLED_STACK_H
