cmake_minimum_required(VERSION 3.15)
project(Processor)

set(CMAKE_CXX_STANDARD 14)
include_directories(../Libraries)
add_executable(assembler assembler.cpp)