# Stack machine interpreter

It is simple Stack-based machine interpreter. Also it can be considered as CPU emulator with own binary OP-codes and asm-like language.

## Stack

Stack is core of this emulator, so it needed to be implemented fast and safe. It located in [Libraries/stack.h](Libraries/stack.h). I applied some instruments to debug and protect this stack from corruption and overflow. Every time when program do something with stack, it checks himself for errors, and report them.

## ASM-dialect

I used Intel-like syntax for my asm, but all comands take their operands from stack. List of all comands located in [keywords](keywords) file. Operands for comands encoded with flags. When this file changing script [generator](Processor/generator.py) creates function defenitions to process new operations. To add new logic it's only need to add hander.

### Flags for comands 
There are 3 flags for every comand:

First flag is arg count flag.

Second - can argument be label

Third - can argument be register

### Memory system

In file [Processor](Processor/stack_processor.cpp) I implemented simple list allocator. It can be used with certain comands to alloc, free and read/write operations.  